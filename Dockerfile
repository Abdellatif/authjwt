FROM openjdk:8-jre-alpine

ADD ./target/AuthJWT.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/AuthJWT.jar"]

EXPOSE 9991
